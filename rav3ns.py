from bs4 import BeautifulSoup
import requests

handle = input('Input your account name on Twitter: ')
temp = requests.get('https://twitter.com/' + handle)
bs = BeautifulSoup(temp.text, 'lxml')
try:
    follow_box = bs.find('li', {'class': 'ProfileNav-item ProfileNav-item--followers'})
    following_box = bs.find('li', {'class': 'ProfileNav-item ProfileNav-item--following'})
    tweet_box = bs.find('li', {'class': 'ProfileNav-item ProfileNav-item--tweets is-active'})

    followers = follow_box.find('a').find('span', {'class': 'ProfileNav-value'})
    following = following_box.find('a').find('span', {'class': 'ProfileNav-value'})
    tweets = tweet_box.find('a').find('span', {'class': 'ProfileNav-value'})

    print("Number of followers: {} ".format(followers.get('data-count')))
    print("Number of following: {} ".format(following.get('data-count')))
    print("Number of tweets: {} ".format(tweets.get('data-count')))

except:
    print('Account name not found...')
