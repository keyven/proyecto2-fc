from bs4 import BeautifulSoup
import requests
user = input('Input your account name on Twitter: ')
bs = BeautifulSoup(requests.get(f'https://twitter.com/{user}').text,'html.parser')
followers = bs.find('li',{'class':'ProfileNav-item ProfileNav-item--followers'}).find('a').find('span',{'class':'ProfileNav-value'}).get('data-count')
following = bs.find('li',{'class':'ProfileNav-item ProfileNav-item--following'}).find('a').find('span',{'class':'ProfileNav-value'}).get('data-count')
tweets = bs.find('li',{'class':'ProfileNav-item ProfileNav-item--tweets is-active'}).find('a').find('span',{'class':'ProfileNav-value'}).get('data-count')
print(f"\nThe account {user} has:\n\nFollowers: {followers}\nFollowing: {following}\nTweets: {tweets}")
