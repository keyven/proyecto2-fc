import requests
from bs4 import BeautifulSoup
src = requests.get('https://twitter.com/barackobama').content
spans = BeautifulSoup(src, 'html.parser').findAll("span", {"class": "ProfileNav-value"})
print('Siguiendo a ' + spans[1].get('data-count') + ' cuentas\r\nTiene ' + spans[2].get('data-count') + ' seguidores\r\nHa escrito ' + spans[0].get('data-count') + ' tweets')
