## PROYECTO II - Forocoches

Estos proyectos se basan en pequeños desafíos para aprender a programar en Python.
Enlace al post: https://www.forocoches.com/foro/showthread.php?t=7931052

## Desafío
·Crear un scraper de Twitter donde extraiga la siguiente información de el usuario que queramos:
-Cantidad de cuentas que está siguiendo
-Cantidad de seguidores que tiene
-Cantidad de tweets realizados


---

## Participantes:

· baloth
· samozite
· Abbath
· Redmoon
· Escopeto
· Rav3ns
· Keyven