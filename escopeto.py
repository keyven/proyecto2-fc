import urllib3
from bs4 import BeautifulSoup

def grab(username, key):
    return BeautifulSoup(urllib3.PoolManager().request('GET', f"https://twitter.com/{username}").data, 'html.parser').find_all('a', {'href': f"/{username}/{key}"})[0]['title'].split(" ")[0]
def count(username):
    return BeautifulSoup(urllib3.PoolManager().request('GET', f"https://twitter.com/{username}").data, 'html.parser').find('li', {'class': 'ProfileNav-item ProfileNav-item--tweets is-active'}).find('a').find('span', {'class': 'ProfileNav-value'}).get('data-count')

username = 'BarackObama'
print(f"Seguidores de {username}: {grab(username, 'following')}")
print(f"{username} sigue a: {grab(username, 'followers')}")
print(f"{username} tiene: {count(username)} tweets")
