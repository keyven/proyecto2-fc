 from bs4 import BeautifulSoup
import requests

usuario = 'BarackObama'
temp = requests.get('https://twitter.com/'+usuario)
bs = BeautifulSoup(temp.text,'lxml')

try:
    siguiendo = bs.find('li',{'class':'ProfileNav-item ProfileNav-item--following'})
    seguidores = bs.find('li', {'class': 'ProfileNav-item ProfileNav-item--followers'})
    cantidadTweets = bs.find('li', {'class': 'ProfileNav-item ProfileNav-item--tweets is-active'})
    sig = siguiendo.find('a').find('span',{'class':'ProfileNav-value'})
    seg = seguidores.find('a').find('span', {'class': 'ProfileNav-value'})
    ct = cantidadTweets.find('a').find('span', {'class': 'ProfileNav-value'})
    print("{} está siguiendo {} cuentas.".format(usuario,sig.get('data-count')))
    print("Seguidores: {} ".format(seg.get('data-count')))
    print("{} ha realizado {} tweets".format(usuario, ct.get('data-count')))

except:
    print('Cuenta no encontrada')
