import requests
from bs4 import BeautifulSoup
 
cuneta = input('Selecciona una cuenta de Twitter: ')
 
req = requests.get( 'https://twitter.com/{0}'.format(cuneta) )
 
soup = BeautifulSoup(req.content, 'html.parser')
numeros = soup.find_all("span", class_="ProfileNav-value")
 
comentarios = ['Ha escrito {0} tweets.','Sigue {0} cuentas.','Tiene {0} seguidores.']
 
print( 'El usuario @{0} :'.format(cuneta) )
for idx in range(len(numeros[:3])):
    numero = numeros[idx]['data-count']
    print(comentarios[idx].format(numero))
